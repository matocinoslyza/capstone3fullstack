import React from 'react';
import {Row, Col, Card} from 'react-bootstrap'
import Button from '@mui/material/Button';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';

export default function Featured() {
    return(
        <>
        <Row>
            <Col className="p-5 text-center">
                        <h2 className="mb-3">Our Products' Specialities</h2>
                        <h3>More than just a look</h3>
                        <p className="my-3 banner-description text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
                        <Button color="primary" variant="contained" size="large" href="/products" endIcon={<AddShoppingCartIcon />}>Buy Now!</Button>

                    </Col>
        </Row>
        <Row>

        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                    <img
                    src="/retro-vives.jpg"
                    width="100%"
                    height="auto"
                    alt="Retro"
                    className="mb-3"
                    />
                            <h2>Aesthetics</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
                        molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
                        numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
                        optio, eaque rerum!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
            <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                        <img
                    src="/vintage.jpg"
                    width="100%"
                    height="auto"
                    alt="Retro"
                    className="mb-3"
                    />
                            <h2>Vintage</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
                        molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
                        numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
                        optio, eaque rerum!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            
            <Col xs={12} md={4}>
            <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                        <img
                    src="/tees.jpg"
                    width="100%"
                    height="auto"
                    alt="Retro"
                    className="mb-3"
                    />
                            <h2>Statement Tees</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
                        molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
                        numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
                        optio, eaque rerum! 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
       </> 
    )
};