import React from 'react';
import {Row, Col} from 'react-bootstrap';
import Button from '@mui/material/Button';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import '../App.css';

export default function Banner() {
    return(
            <Row className="d-flex align-items-center">
                <Col xs={12} md={6} className="p-5">
                    <h1 className="mb-3">Aesthetics Shirts Store</h1>
                    <p className="my-3 banner-description">Enjoy the wide variety of cool aesthetic designs that you can use or wear proudly. Express your originality, wear these visually appealing designs.</p>
                    <Button color="primary" variant="contained" size="large" href="/products" endIcon={<AddShoppingCartIcon />}>Buy Now!</Button>
                </Col>
                <Col xs={12} md={6} className="p-5 hero-img">
                <img
                    src="/hero-img.jpg"
                    width="100%"
                    height="auto"
                    alt="Hero-Img"
            />
                </Col>
            </Row>
    )
};