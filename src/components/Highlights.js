import React from 'react';
import {Row, Col, Card} from 'react-bootstrap'
import { FaBlackTie } from "react-icons/fa";
import { FaTshirt } from "react-icons/fa";
import { FaAward } from "react-icons/fa";
import { FaTags } from "react-icons/fa";
import Button from '@mui/material/Button';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
export default function Highlights() {
    return(
        <>
        <Row>
            <Col className="p-5 text-center">
                        <h2 className="mb-3">Welcome to the Aesthetics Shirts store</h2>
                        <p className="my-3 banner-description text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
                        <Button color="primary" variant="contained" size="large" href="/products" endIcon={<AddShoppingCartIcon />}>Buy Now!</Button>

                    </Col>
            </Row>
        <Row>
            <Col xs={12} md={3}>
                <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                            <FaBlackTie className="mb-3"
                                      size="100px"
                                      color="#d5a494"
                            />
                            <h3>Unique Design</h3>
                        </Card.Title>
                        <Card.Text>
                        Suspendisse posuere, diam in bibendum posuere, diam
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            
            <Col xs={12} md={3}>
            <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                        <FaTshirt className="mb-3"
                                      size="100px"
                                      color="#d5a494"
                            />
                            <h3>Perfect fit clothes</h3>
                        </Card.Title>
                        <Card.Text>
                        Suspendisse posuere, diam in bibendum posuere, diam
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
            <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                        <FaAward className="mb-3"
                                      size="100px"
                                      color="#d5a494"
                            />
                            <h3>Original Quality</h3>
                        </Card.Title>
                        <Card.Text>
                        Suspendisse posuere, diam in bibendum posuere, diam
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
            <Card className="cardHighlight p-3 text-center">
                    <Card.Body>
                        <Card.Title>
                        <FaTags className="mb-3"
                                      size="100px"
                                      color="#d5a494"
                            />
                            <h3>Competitive Prices</h3>
                        </Card.Title>
                        <Card.Text>
                        Suspendisse posuere, diam in bibendum posuere, diam
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
       </> 
    )
};