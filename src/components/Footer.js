import React from 'react';
import {Row, Col} from 'react-bootstrap';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import PinterestIcon from '@mui/icons-material/Pinterest';

export default function Footer() {
    return(
        <>
        <Row xpand="lg" variant="dark" className="color-nav text-white p-5">
            <Col xs={12} md={4} className="p-3">   
            <img
                src="/logo.jpg"
                width="70"
                height="70"
                className="d-inline-block align-top logo mb-3"
                alt="Logo"
            />
                <h4 className="mb-3 footer-header">Aesthetics Shirts Store</h4>
                <p className="my-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                <div className="footer-icons">
				<div><FacebookIcon to="https://www.facebook.com/CoolPhils"/></div>
				<div><InstagramIcon/></div>
				<div><PinterestIcon to="https://pin.it/50Ppjup/"/></div>
			</div>

            </Col>
            <Col xs={12} md={4} className="p-3 text-white">
                        <h4 className="mb-3 footer-header">Our Services</h4>
                        <p className="my-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                        <p>This is a link</p>
                        <p>This is a link</p>
            </Col>
            <Col xs={12} md={4} className="p-3 text-white">
                        <h4 className="mb-3 footer-header">About Us</h4>
                        <p className="my-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                        <p>This is a link</p>
                        <p>This is a link</p>
            </Col>
        </Row>
        <Row className="p-3">
            <p className="text-center mb-0">© 2022 AestheticShirtStore.com. All rights reserved.</p>
        </Row>
       </> 
    )
};