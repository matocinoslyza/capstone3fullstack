import React from 'react';
import { Card, Col} from 'react-bootstrap';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import VisibilityIcon from '@mui/icons-material/Visibility';

export default function ProductCard({productProp}) {

	//Deconstruct the productProp into their own variables
	const { _id, name, description, price } = productProp;


	return(
		// <Card className="m-3 d-md-inline-flex d-sm-inline-flex cardForm text-center">
		<Col className="products-card">
		<Card className="cardHighlight p-3 text-center">
			<Card.Body className="consistent-width">
				<Card.Title> { name } </Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text> { description } </Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php { price }</Card.Text>
				<Button variant="contained" color="warning" endIcon={<VisibilityIcon />} href={`/products/${_id}`}>View Product</Button>
			</Card.Body>
		</Card>
		</Col>
)
}

//Check if the ProductCard component is getting the correct prop types
//PropTypes are used for validationg information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
ProductCard.propTypes = {
	//shape() method it is used to check if a prop object conforms to a specific 'shape'
	productProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}









