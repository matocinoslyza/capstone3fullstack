import React, { useContext } from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import '../App.css';

export default function AppNavbar() {
    const { user  } = useContext(UserContext);


  
    return(
        <>
        <Navbar expand="lg" variant="dark" className="color-nav">
            <Navbar.Brand href="#" className="ms-2"></Navbar.Brand>
            <Link to="/">
            <img
                src="/logo.jpg"
                width="50"
                height="50"
                className="d-inline-block align-top logo"
                alt="Logo"
            />
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className=" ms-auto">
                    <Nav.Link className="nav-link-color" as={ Link } to="/">Home</Nav.Link>
                    <Nav.Link className="nav-link-color" as={ Link } to="/products">Products</Nav.Link>

                
                    { (user.accessToken !== null) ?
                    <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
                        :
                        <>
                            <Nav.Link className="nav-link-color" as={ Link } to="/login">Login</Nav.Link>
                            <Nav.Link className="nav-link-color" as={ Link } to="/register">Register</Nav.Link>
                        </>
                }
                
                </Nav>
            </Navbar.Collapse>
        </Navbar>
        </>
    )
};

