import React, { useState } from 'react';
import { Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';
import CancelIcon from '@mui/icons-material/Cancel';


export default function DeleteProduct({product, fetchData}) {

	//state for productId for the fetch URL
	const [productId, setProductId] = useState('');

	//Forms state
	//Add state for the forms of product
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	//state for deleteProduct Modals to open/close
	const [showDelete, setShowDelete] = useState(false)

	//function for opening the modal
	const openDelete = (productId) => {
		//to still get the actual data from the form
		fetch(`https://quiet-falls-98609.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			//populate all the input values with product info that we fetched
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})

		//Then, open the modal
		setShowDelete(true)
	}

	const closeDelete = () => {
		setShowDelete(false);
		setName('')
		setDescription('')
		setPrice(0)
	}

	//function to edit the product
	const deleteProduct = (e, productId) => {
		e.preventDefault();

		fetch(`https://quiet-falls-98609.herokuapp.com/products/${ productId }`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Successfully Deleted'
				})
				closeDelete();
				fetchData();
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
				closeDelete();
				fetchData();
			}
		})
	}






	return(
		<>
			<Button variant="contained" color="error" onClick={() => openDelete(product)} endIcon={<DeleteIcon />}>Delete</Button>

		{/*Delete Modal Forms*/}
			<Modal show={showDelete} onHide={closeDelete}>
				<Form onSubmit={e => deleteProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Delete Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
                        <div>Are you sure you want to delete this {name}?</div>
					</Modal.Body>

					<Modal.Footer className="mb-3">
						<Button style={{margin: 10}} variant="contained" color="secondary"  endIcon={<CancelIcon />}  onClick={closeDelete}>No</Button>
						<Button variant="contained" color="success"  type="submit" endIcon={<SendIcon />}>Yes, I want to delete</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>
		)
}