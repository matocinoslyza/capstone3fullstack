import React, { useState, useEffect } from 'react';
// import { Table } from 'react-bootstrap';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import DeleteProduct from './DeleteProduct';
import ArchiveProduct from './ArchiveProduct';




export default function AdminView(props) {

	const { productsData, fetchData } = props;
	const [products, setProducts] = useState([])

	//=============Getting the productsData from the products page
	useEffect(() => {
		const productsArr = productsData.map(product => {
			return (
				<TableRow key={product._id}>
					<TableCell className="table-cell">{product._id}</TableCell>
					<TableCell className="table-cell">{product.name}</TableCell>
					<TableCell className="table-cell">{product.description}</TableCell>
					<TableCell className="table-cell">{product.price}</TableCell>
					<TableCell alignCenter className= {product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Available" : "Unavailable"}
					</TableCell>
					<TableCell className="table-cell"><EditProduct product={product._id} fetchData={fetchData}/></TableCell>
					<TableCell className="table-cell"><DeleteProduct product={product._id} fetchData={fetchData}/></TableCell>
					<TableCell className="table-cell"><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/></TableCell>		
				</TableRow>


				)
		})

		setProducts(productsArr)

	}, [productsData])



	//=============end of useEffect for productsData


	return(
		<>
			<div className="text-center my-4">
				<h1> Admin Dashboard</h1>
				<AddProduct fetchData={fetchData}/>
			</div>
			<TableContainer component={Paper} align="center" className="mb-5">
			<Table sx={{ minWidth: 650 }} aria-label="simple table" align="center" className="table-color">
				<TableHead align="center" className="text-white">
					<TableRow align="center">
						<TableCell className="table-header">ID</TableCell>
						<TableCell className="table-header">Name</TableCell>
						<TableCell className="table-header">Description</TableCell>
						<TableCell className="table-header">Price</TableCell>
						<TableCell className="table-header">Availability</TableCell>
						<TableCell className="table-header" colSpan="3">Actions</TableCell>
					</TableRow>
				</TableHead>

				<TableBody align="center" className="text-white">
					{products}
				</TableBody>
			</Table>
			</TableContainer>
			
		</>

		)
}