import React from 'react';
import {Row, Col} from 'react-bootstrap'
import Button from '@mui/material/Button';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';

export default function Limited() {
    return(
        <>
        <Row>
            <Col className="p-5 text-center">
                        <h2 className="mb-3">Limited Time Online</h2>
                        <p className="my-3 banner-description text-center">Click & Collect Next Day Delivery Free Over 20$</p>
                        <Button color="primary" variant="contained" size="large" href="/products" endIcon={<AddShoppingCartIcon />}>Buy Now!</Button>

                    </Col>
        </Row>
       </> 
    )
};