import React from 'react';
// import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Button from '@mui/material/Button';
import ArchiveIcon from '@mui/icons-material/Archive';
import UnarchiveIcon from '@mui/icons-material/Unarchive';

export default function ArchiveProduct({product, isActive, fetchData}) {

	const archiveToggle = (productId) => {
		fetch(`https://quiet-falls-98609.herokuapp.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}


		})
	}
 
    // reactivate
    const unarchiveToggle = (productId) => {
		fetch(`https://quiet-falls-98609.herokuapp.com/products/${productId}/reactivate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === false) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully re-enabled'
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}


		})
	}

	return(
		<>
			{isActive ?

				<Button variant="contained" color="error" onClick={() => archiveToggle(product)} endIcon={<ArchiveIcon />}>Archive</Button>

				:

				<Button variant="contained" color="success" onClick={() => unarchiveToggle(product)} endIcon={<UnarchiveIcon />}>Unarchive</Button>

			}
		</>


		)
}
