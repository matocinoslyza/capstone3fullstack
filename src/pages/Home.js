import React from 'react';
import Banner from '../components/Banner';
import Featured from '../components/Featured';
import Highlights from '../components/Highlights';
import Limited from '../components/Limited';

export default function Home() {
    return(
        <>
            <Banner />
            <Highlights />
            <Featured />
            <Limited />
        </>
    )
}
