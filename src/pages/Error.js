import React from 'react';

export default function Error() {

    localStorage.clear()

    return(
        <div id="wrapper">
        <div id="info">
            <h1>Page Not Found</h1>
            <p>Go back to the <a href="/">homepage</a></p>
        </div>
        </div >
    )
}
