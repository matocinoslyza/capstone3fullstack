import React, { useState, useEffect, useContext } from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Button from '@mui/material/Button';
import LoginIcon from '@mui/icons-material/Login';

export default function Login() {
	const navigate = useNavigate();
	const { user, setUser } = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');

	//State to determine whether submit button is enabled or not for conditional rendering
	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '')){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [email, password1])

	// Notes
	// fetch('url', {options}.then(response => response.json()).then(data => {
	// 	console.log(data.)
	// }) )


	function authenticate(e) {
		//Prevents page redirection via form submission
		e.preventDefault();
		fetch('https://quiet-falls-98609.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password1: password1
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });
				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Successfully Logged In'
				})

				fetch('https://quiet-falls-98609.herokuapp.com/users/getUserDetails', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true) {
						localStorage.setItem('email', result.email)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})

						// //redirect the admin to /products
						// navigate('/products')

						

					}else {
						//if not an admin, redirect to homepage
						navigate('/')
					}
				})

			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Something went wrong. Check your credentials.'
				})
			}
			setEmail('');
			setPassword1('');
		})
	}

	return (   
		(user.accessToken != null) ?
		<Navigate to="/products" />
		:

<Row className="login-row">
	<Col>
	<Form className="mb-5" onSubmit={(e) => authenticate(e)}>
		<h2>Login</h2>
		<Form.Group> 
			<Form.Label>Email Adress</Form.Label>
			<Form.Control 
				type="email"
				placeholder="Enter Email"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
		</Form.Group>

		<Form.Group> 
			<Form.Label>Password</Form.Label>
			<Form.Control 
				type="password"
				placeholder="Enter Password"
				required
				value={password1}
				onChange={e => setPassword1(e.target.value)}
				/>
		</Form.Group>
		{isActive ? 
			<Button variant="contained" color="primary" type="submit" className="mt-3" endIcon={<LoginIcon />}>Login</Button>

			:

			<Button variant="contained" color="primary" type="submit" className="mt-3" disabled endIcon={<LoginIcon />}>Login</Button>

		}
		
	</Form>
	</Col>
	</Row>
		)
}
