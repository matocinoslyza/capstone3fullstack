import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Col} from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Button from '@mui/material/Button';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';

export default function SpecificProduct() {

	const navigate = useNavigate();

	//useParams() contains any values we are tryilng to pass in the URL stored
	//useParams is how we receive the productId passed via the URL
	const { productId } = useParams();

	useEffect(() => {

		fetch(`https://quiet-falls-98609.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	} ,[])

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')



	//enroll function
	const enroll = (productId) => {
		fetch('https://quiet-falls-98609.herokuapp.com/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Successfully enrolled',
					icon: 'success'
				})

				navigate('/products')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		})
	}


	return (
		<Container className="m-5">
			<Col xs={12} md={4} className="text-center specific-product-card">
				<Card>
					<Card.Header>
						<h4>{name}</h4>
					</Card.Header>

					<Card.Body>
						<Card.Text>{description}</Card.Text>
						<h6>Price: Php {price}</h6>
					</Card.Body>

					<Card.Footer>

						{user.accessToken !== null ?
							<div className="d-grip gap-2">
								<Button variant="contained" color="secondary" onClick={() => enroll(productId)} endIcon={<AddShoppingCartIcon />}>Order</Button>
							</div>

							:

							<Button variant="contained" color="warning" className="btn btn-warning d-grip gap-2" href="/login" endIcon={<AddShoppingCartIcon />}>Login to Order</Button>
						}
						
					</Card.Footer>
				</Card>
				</Col>
		</Container>

		)
}


