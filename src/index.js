import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';

// const name = 'John Smith';
// let job = "Proffresional Driver";

// // const element = (
// //   <>
// //     <h1>Hello, {name}</h1>
// //     <h3>Full Stack Developer</h3>
// //   </>
// // )

// let personDisplay1 = (
//   <>
//     <h1>{name}</h1>
//     <h2>{job}</h2>
//   </>
// )

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
<React.StrictMode>
    <App />
</React.StrictMode>


);
